import React from 'react';
import css from './styles.css';

export function App() {
  return <div className={css.container}>
    <h1>Hello world!</h1>
  </div>;
}