import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './components/Routes';

// if #root doesn't exist, create a new one and inject into the DOM
let root = document.getElementById('root');
if (!root) {
  root = document.createElement('div');
  document.body.appendChild(root);
}

ReactDOM.render(<Routes />, root);