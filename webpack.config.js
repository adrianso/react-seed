const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const precss = require('precss');

// plugins
const CleanPlugin = require('clean-webpack-plugin');

const PATHS = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist'),
};

const port = 8080;

module.exports = {
  port,
  
  // where does Webpack start building?
  entry: [
    'webpack-dev-server/client?http://localhost:' + port,
    'webpack/hot/only-dev-server',      
    './src/main',
  ],
  
  // where should Webpack store it processed files
  output: {
    path: PATHS.dist,
    filename: 'bundle.js',
  },
  
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['react-hot', 'babel'],
      exclude: /(node_modules|vendor)/,
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader?modules&importLoaders=1!postcss-loader'
    }]    
  },
  
  postcss: function() { 
    return [autoprefixer({
      browsers: ['last 2 versions'],
    }), precss];
  },
  
  plugins: [
    // hot module replacement
    new webpack.HotModuleReplacementPlugin(),
    
    // clean dist directory on build
    new CleanPlugin('./dist'),
    
    // make sure build stops when it encounter an error
    new webpack.NoErrorsPlugin(),
  ],
  
  devtool: 'eval-source-map',  
};